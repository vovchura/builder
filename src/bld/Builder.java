package bld;

 class Builder {
      String name;
     String colour;
      int speed;
      int cost;

     public Builder(String name) {
         this.name = name;
     }

     public Builder setColour(String colour) {
         this.colour = colour;
     return this;
     }

     public Builder setSpeed(int speed) {
         this.speed = speed;
     return this;
     }

     public Builder setCost(int cost) {
         this.cost = cost;
     return this;

     }
 }
