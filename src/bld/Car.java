package bld;

public class Car {
    private String name;
    private String colour;
    private int speed;
    private int cost;

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }

    public int getSpeed() {
        return speed;
    }

    public int getCost() {
        return cost;
    }

    private Car(Builder builder) {
        this.name = builder.name;
        this.colour = builder.colour;
        this.cost = builder.cost;
        this.speed = builder.speed;

    }

    static class Builder {
        String name;
        String colour;
        int speed;
        int cost;

        public Builder(String name) {
            this.name = name;
        }

        public Builder setColour(String colour) {
            this.colour = colour;
            return this;
        }

        public Builder setSpeed(int speed) {
            this.speed = speed;
            return this;
        }

        public Builder setCost(int cost) {
            this.cost = cost;
            return this;

        }
    public Car build(){
    return new Car(this);
    }
    }
}
